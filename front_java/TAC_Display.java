public TextView txtNom;
public TextView txtPrenom;
public TextView txtDTN;
public TextView txtResultPass;
String responseTACString;
	
txtNom = (TextView) findViewById(R.id.txtNom);
txtPrenom = (TextView) findViewById(R.id.txtPrenom);
txtDTN = (TextView) findViewById(R.id.txtDTN);
txtResultPass = (TextView) findViewById(R.id.txtResultPass);	

public Integer getAPITAC(String sQrCode){

		responseTACString = "";
		// no error
		Integer nError = 0;
		try {

				// URL production
				URL url = new URL("http://test.dag-system.com:8083/covid/passValidation");

				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("POST");
				conn.setRequestProperty("appId","XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
				conn.setRequestProperty("appPassword","XXXXXXXXXXXXXXXX");
				conn.setRequestProperty("Content-type","application/json");
				conn.setDoInput(true);
				conn.setDoOutput(true);
				OutputStream os = conn.getOutputStream();
				BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));

				JSONObject bodyTAC = new JSONObject();
				bodyTAC.put("certificate", sQrCode);
				writer.write(bodyTAC.toString());

				writer.flush();
				writer.close();
				os.close();
				int responseCode=conn.getResponseCode();
				String line;

				if (responseCode == HttpsURLConnection.HTTP_OK) {
						// no error => read data
						BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
						while ((line=br.readLine()) != null) {
								responseTACString+=line;
						}
				}
				else {
						// http error => read error
						nError = 2;
						BufferedReader br=new BufferedReader(new InputStreamReader(conn.getErrorStream()));
						while ((line=br.readLine()) != null) {
								responseTACString+=line;
						}

				}
		} catch (Exception e) {
				nError = 1;
				responseTACString=e.toString();
		}

		return nError;
}

// check anticovid avec QR COde lu par l'appareil photo
switch(getAPITAC(QRCode)){
	case 0:     // http ok
		// reponse ok
		boolean isBlacklisted = false;
		String liteValidityStatus = "";
		String liteFirstName = "";
		String liteLastName = "";
		String liteDateOfBirth = "";
		try {
			JSONObject jsoAPITAC = new JSONObject(responseTACString); // convert String to JSONObject
			String sDataAPITAC = jsoAPITAC.getString("data");
			JSONObject jsoDataAPITAC = new JSONObject(sDataAPITAC);
			JSONArray jsaDynamicAPITAC = jsoDataAPITAC.getJSONArray("dynamic");
			if (jsaDynamicAPITAC.length() > 0) {
				JSONObject jsoDataDynamicAPITAC = jsaDynamicAPITAC.getJSONObject(0);
				liteValidityStatus = jsoDataDynamicAPITAC.getString("liteValidityStatus");
				liteFirstName = jsoDataDynamicAPITAC.getString("liteFirstName");
				liteLastName = jsoDataDynamicAPITAC.getString("liteLastName");
				liteDateOfBirth = jsoDataDynamicAPITAC.getString("liteDateOfBirth");
			}
			String sStaticAPITAC = jsoDataAPITAC.getString("static");
			JSONObject jsoStaticAPITAC = new JSONObject(sStaticAPITAC);
			isBlacklisted = jsoStaticAPITAC.getBoolean("isBlacklisted");

			String sExternalAPITAC = jsoDataAPITAC.getString("external");

			if (isBlacklisted == true) {
				// blacklisté > Affiche résultat + message
				txtResultPass.setText("Pass sanitaire : " + liteValidityStatus + "\r\n\n" +
						"Ce pass sanitaire n’est pas considéré comme valide. La consigne est de refuser l’accès au titulaire de ce pass sanitaire.\n" +
						"Le QR Code que vous venez de scanner a été signalé comme frauduleux et n’est pas recevable au titre de pass sanitaire.\n" +
						"Si l’usager est le propriétaire du pass révoqué, il peut facilement regénérer un certificat de vaccination sur attestation-vaccin.ameli.fr ou de test sur sidep.gouv.fr.\n" +
						"En cas de contrôle par les forces de l’ordre, l’utilisation frauduleuse d’un pass sanitaire appartenant à autrui est passible de 45 000 € et 3 ans d’emprisonnement.\n" +
						"Pour tout information, l’usager peut contacter le support au 0800 08 71 48.");
			}else{
				// pas blacklisté > Affiche résultat
				txtResultPass.setText("Pass sanitaire : " + liteValidityStatus + "\r\n\n");
			}
			txtResultPass.setVisibility(View.VISIBLE);

			txtNom.setText(liteLastName);
			txtNom.setVisibility(View.VISIBLE);
			txtPrenom.setText(liteFirstName);
			txtPrenom.setVisibility(View.VISIBLE);
			txtDTN.setText(liteDateOfBirth);
			txtDTN.setVisibility(View.VISIBLE);

			// affiche image OK ou KO
			ImageView imgPhoto = (ImageView) findViewById(R.id.ivPhoto);
			imgPhoto.setVisibility(View.VISIBLE);
			if ((liteValidityStatus.equals("Valide") == false) || (isBlacklisted == true)) {
				// pass non valide ou blacklisté > affiche image KO
				imgPhoto.setImageResource(R.drawable.ko_64x64);
			} else {
				// pass valide > affiche image OK
				imgPhoto.setImageResource(R.drawable.ok_64x64);
			}

			// affiche bouton nouveau scan
			Button btnResult = (Button) findViewById(R.id.btnResult);
			btnResult.setText("NOUVEAU SCAN");
			btnResult.setBackgroundColor(Color.LTGRAY);

		} catch (Exception e) {
			// erreur QRCode > affiche toast d'alerte
			toast("Erreur QRCode", TOAST_ERROR, R.drawable.writeerror);
		}
		break;
	case 2:     // http error
		try {
			JSONObject jsoErrorAPITAC = new JSONObject(responseTACString); // convert String to JSONObject
			String sErrorAPITAC = jsoErrorAPITAC.getString("message");
			txtResultPass.setText("Erreur : \r\n" + sErrorAPITAC + "\r\n\n" + responseTACString + "\r\n\n");
			txtResultPass.setVisibility(View.VISIBLE);
		} catch (Exception e) {
			toast("Erreur QRCode", TOAST_ERROR, R.drawable.writeerror);
		}

		break;
	default:    // exception
		txtResultPass.setText("Erreur : \r\n" + responseTACString + "\r\n\n");
		txtResultPass.setVisibility(View.VISIBLE);
		break;
}