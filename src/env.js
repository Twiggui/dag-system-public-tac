require("dotenv").config();

function getEnv(variable) {
  const value = process.env[variable];
  if (typeof value === "undefined") {
    console.warn(`Seems like the variable "${variable}" is not set in the environment. 
    Did you forget to execute "cp .env.sample .env" and adjust variables in the .env file to match your own environment ?`);
  }
  return value;
}

const SERVER_ADDRESS = getEnv("SERVER_ADDRESS");
const SERVER_PORT = parseInt(getEnv("SERVER_PORT"));

const DB_HOST = getEnv(`DB_HOST`);
const DB_PORT = getEnv(`DB_PORT`);
const DB_USER = getEnv(`DB_USER`);
const DB_PASS = getEnv(`DB_PASS`);
const DB_NAME = getEnv(`DB_NAME`);

const DB_HOST_00 = getEnv(`DB_HOST_00`);
const DB_PORT_00 = getEnv(`DB_PORT_00`);
const DB_USER_00 = getEnv(`DB_USER_00`);
const DB_PASS_00 = getEnv(`DB_PASS_00`);
const DB_NAME_00 = getEnv(`DB_NAME_00`);

const DB_HOST_22 = getEnv(`DB_HOST_22`);
const DB_PORT_22 = getEnv(`DB_PORT_22`);
const DB_USER_22 = getEnv(`DB_USER_22`);
const DB_PASS_22 = getEnv(`DB_PASS_22`);
const DB_NAME_22 = getEnv(`DB_NAME_22`);

const MONGODB_HOST = getEnv(`MONGODB_HOST`);
const MONGODB_PORT = getEnv(`MONGODB_PORT`);
const MONGODB_NAME = getEnv(`MONGODB_NAME`);
const MONGODB_USER = getEnv(`MONGODB_USER`);
const MONGODB_PASSWORD = getEnv(`MONGODB_PASSWORD`);
const MONGODB_OPTIONS = getEnv(`MONGODB_OPTIONS`);

const FTP_HOST = getEnv(`FTP_HOST`);
const FTP_PORT = Number.parseInt(getEnv(`FTP_PORT`));
const FTP_USER = getEnv(`FTP_USER`);
const FTP_PASS = getEnv(`FTP_PASS`);
const FTP_FOLDER = getEnv(`FTP_FOLDER`);

const JWT_PRIVATE_KEY = getEnv(`JWT_PRIVATE_KEY`);
const JWT_EXPIRES_IN = getEnv(`JWT_EXPIRES_IN`);
const JWT_EXPIRES_IN_REMEMBER = getEnv(`JWT_EXPIRES_IN_REMEMBER`);

const API_URL_DAG_SYSTEM = getEnv(`API_URL_DAG_SYSTEM`);
const API_URL_DAG_SYSTEM_VERSION = getEnv(`API_URL_DAG_SYSTEM_VERSION`);
const URL_FRONT_BACK_OFFICE = getEnv(`URL_FRONT_BACK_OFFICE`);

const MAILFORYOU_BASEURL = getEnv(`MAILFORYOU_BASEURL`);
const MAILFORYOU_CLIENT = getEnv(`MAILFORYOU_CLIENT`);
const MAILFORYOU_KEY = getEnv(`MAILFORYOU_KEY`);

const LOG_TCP_HOST = getEnv(`LOG_TCP_HOST`);
const LOG_TCP_PORT = getEnv(`LOG_TCP_PORT`);
const TEMPLATE_REPO = getEnv(`TEMPLATE_REPO`);
const API_URL = getEnv(`API_URL`);
const SERVER_APPS_DEMO_FOLDER_PATH = getEnv(`SERVER_APPS_DEMO_FOLDER_PATH`);
const APK_LINK_PATH = getEnv(`APK_LINK_PATH`);

const PASSWORD_RECOVERY_ENCRYPTION_KEY = getEnv(
  "PASSWORD_RECOVERY_ENCRYPTION_KEY"
);

const ENCRYPTION_KEY = getEnv("ENCRYPTION_KEY");

const GENERATED_GPX_PATH = getEnv("GENERATED_GPX_PATH");
const UPLOADED_GPX_PATH = getEnv("UPLOADED_GPX_PATH");
const GPX_FOLOMI_URL = getEnv("GPX_FOLOMI_URL");
const IS_USING_FOLOMI = getEnv("IS_USING_FOLOMI");

module.exports = {
  SERVER_ADDRESS,
  SERVER_PORT,
  DB_HOST,
  DB_PORT,
  DB_USER,
  DB_NAME,
  DB_PASS,

  DB_HOST_00,
  DB_PORT_00,
  DB_USER_00,
  DB_NAME_00,
  DB_PASS_00,

  DB_HOST_22,
  DB_PORT_22,
  DB_USER_22,
  DB_NAME_22,
  DB_PASS_22,

  MONGODB_HOST,
  MONGODB_PORT,
  MONGODB_NAME,
  MONGODB_USER,
  MONGODB_PASSWORD,
  MONGODB_OPTIONS,

  FTP_HOST,
  FTP_PORT,
  FTP_USER,
  FTP_PASS,
  FTP_FOLDER,

  JWT_PRIVATE_KEY,
  JWT_EXPIRES_IN,
  JWT_EXPIRES_IN_REMEMBER,
  MAILFORYOU_BASEURL,
  MAILFORYOU_CLIENT,
  MAILFORYOU_KEY,

  LOG_TCP_HOST,
  LOG_TCP_PORT,
  TEMPLATE_REPO,
  API_URL_DAG_SYSTEM,
  API_URL_DAG_SYSTEM_VERSION,
  URL_FRONT_BACK_OFFICE,
  API_URL,
  SERVER_APPS_DEMO_FOLDER_PATH,
  APK_LINK_PATH,

  PASSWORD_RECOVERY_ENCRYPTION_KEY,
  ENCRYPTION_KEY,

  GENERATED_GPX_PATH,
  UPLOADED_GPX_PATH,
  GPX_FOLOMI_URL,
  IS_USING_FOLOMI,
};
