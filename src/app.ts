import cors from "cors";
import express from "express";
import Env from "./env";
import { handleError } from "./helper/error";
const app = express();
app.set("trust proxy", 1);
const path = require("path");
var bodyParser = require("body-parser");
const {
  baseImageCreation,
} = require("./controllers/sportApplication/appCreation/terminalFunctions");

export const globalApplicationsRoot =
  process.platform === "win32"
    ? path.resolve(__dirname).split("build\\src")[0]
    : path.resolve(__dirname).split("build/src")[0];

app.use(cors());
app.use(function (err: any, req: any, res: any, next: any) {
  // Website you wish to allow to connect
  res.setHeader("Access-Control-Allow-Origin", "*");
  // Request methods you wish to allow
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );

  // Request headers you wish to allow
  res.setHeader("Access-Control-Allow-Headers", "content-type");

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader("Access-Control-Allow-Credentials", false);
  res.setHeader("content-type", "application/json");

  next();
  next(err);
  // console.log(util.inspect(err, false, null));
  res.status(500).send("Something broke!");
});

// app.use(express.json());
// app.use(express.urlencoded({ extended: true }));

app.use(bodyParser.json({ limit: "50mb" }));
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));

// routes
require("./routes")(app);

app.set("x-powered-by", false);

app.use((err: any, req: any, res: any, next: any) => {
  if (res.name == "next") {
    // appel ne correspondant à aucune route => l'objet res est en fait dans l'objet req car err n'existe pas
    return req.status(404).json({ error: "not found" });
  } else {
    handleError(err, req, res, next);
  }
});

// Creation de l'image de base de logo = image transparente qui s'appelle ic_launcher.png
baseImageCreation();

let server: any = null;

server = app.listen(Env.SERVER_PORT, function () {
  if (process.env.NODE_ENV !== "test") {
    console.log("Listening on port " + Env.SERVER_PORT);
  }
});

export default server;
function err(
  err: any,
  req: any,
  res: any
): import("express-serve-static-core").RequestHandler<
  import("express-serve-static-core").ParamsDictionary,
  any,
  any,
  import("qs").ParsedQs,
  Record<string, any>
> {
  throw new Error("Function not implemented.");
}

module.exports = app;
