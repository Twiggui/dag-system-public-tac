import { Router } from "express";
import CovidController from "../../controllers/commonAllApplications/covid";

const router = Router();
const asyncHandler = require("express-async-handler");

// ***** PUBLIC ROUTES *****
router.post("/passValidation", asyncHandler(CovidController.validatePass));
router.post(
  "/passValidation/test",
  asyncHandler(CovidController.validatePassDebug)
);

// ***** PROTECTED ROUTES (user logged) *****
export default router;
