import covidRouter from "./covid/covid";

const asyncHandler = require("express-async-handler");

module.exports = (app: any) => {
  app.use("/covid", asyncHandler(covidRouter));
};
