/* eslint-disable no-undef */
// @ts-nocheck

import knex from "knex";
import mongoose from "mongoose";
import { ErrorHandler } from "../helper/error";
import modeltemplate from "../models/modeltemplate";

export default class Template {
  static controllerTemplate = async (req: any, res: any, next: any) => {
    const dbCX = knex({
      client: "mysql",
      connection: req.accessDatabases.connectionOptions,
      pool: { min: 0, max: 5 },
    });

    const folomi = knex({
      client: "mysql",
      connection: req.accessDatabases.folomiConnectionOptions,
      pool: { min: 0, max: 5 },
    });

    const mongo = await mongoose.createConnection(
      req.accessDatabases.mongoConnectionOptions,
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
      }
    );

    try {
      if (errorInputCondition) {
        throw new ErrorHandler(400, "input error message");
      }

      const result = await modelServices.function(
        dbCX / folomi / mongo,
        req.body / req.params / req.query
      );

      if (result && errorResultCondition) {
        throw new ErrorHandler(204 / 404, "result error message");
      }

      if (result) {
        dbCX.destroy();
        folomi.destroy();
        mongoose.disconnect();
        return res.status(200).json(result);
      }
    } catch (error) {
      dbCX.destroy();
      folomi.destroy();
      mongoose.disconnect();
      return next(error);
    }
  };
}
