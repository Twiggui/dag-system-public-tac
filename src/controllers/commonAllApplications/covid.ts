import axios from "axios";
import { ErrorHandler } from "../../helper/error";

export default class CovidController {
  static validatePass = async (req: any, res: any, next: any) => {
    type ApiResponse = {
      data: {
        data: {
          dynamic: [
            {
              liteValidityStatus: string;
              liteFirstName: string;
              liteLastName: string;
              liteDateOfBirth: string;
            }
          ];
          static: {
            header: unknown;
            message: unknown;
            annexe: unknown;
            isBlacklisted: boolean;
            signature: unknown;
          };
          external: [];
        };
        errors: [];
      };
      response?: {
        status: number;
        statusText: string;
      };
    };
    const tousAntiCovidTestDCC = async (
      certificate: string
    ): Promise<ApiResponse> => {
      try {
        const result: ApiResponse = await axios.post(
          process.env.TOUSANTICOVID_URL_DCC,
          certificate,
          {
            headers: {
              Authorization: `Bearer ${process.env.TOUSANTICOVID_DAG_TOKEN}`,
              "Content-Type": "text/plain",
            },
          }
        );
        return result;
      } catch (error) {
        return error;
      }
    };
    const tousAntiCovidTest2DDOC = async (
      certificate: string
    ): Promise<ApiResponse> => {
      try {
        const result: ApiResponse = await axios.post(
          process.env.TOUSANTICOVID_URL_2DDOC,
          certificate,
          {
            headers: {
              Authorization: `Bearer ${process.env.TOUSANTICOVID_DAG_TOKEN}`,
              "Content-Type": "text/plain",
            },
          }
        );
        return result;
      } catch (error) {
        return error;
      }
    };

    try {
      const certificate: string = req.body.certificate;
      let result: ApiResponse;
      result = await tousAntiCovidTestDCC(certificate);

      if (result.response) {
        if (result.response.status === 403) {
          throw new ErrorHandler(403, "tousAntiCovid pass outdated");
        }
        if (result.response.status === 404) {
          result = await tousAntiCovidTest2DDOC(certificate);
          if (result.response.status === 404) {
            throw new ErrorHandler(404, "2D-DOC or DCC format is not valid");
          }
        }
      }

      return res.status(200).send(result.data);
    } catch (error) {
      return next(error);
    }
  };

  static validatePassDebug = async (req: any, res: any, next: any) => {
    type ApiResponse = {
      resourceType: string;
      data: {
        dynamic: [
          {
            liteValidityStatus: string;
            liteFirstName: string;
            liteLastName: string;
            liteDateOfBirth: string;
          }
        ];
        static: {
          header: unknown;
          message: unknown;
          annexe: unknown;
          isBlacklisted: boolean;
          signature: unknown;
        };
        external: [];
      };
      errors: [];
      response?: {
        status: number;
        statusText: string;
      };
    };

    try {
      const debugDisplay = req.body.debug;

      if (debugDisplay === 0) {
        return await CovidController.validatePass(req, res, next);
      }
      if (debugDisplay === 1) {
        const response: ApiResponse = {
          resourceType: "DCC_VACCINATION",
          data: {
            dynamic: [
              {
                liteValidityStatus: "Valide",
                liteFirstName: "MICHEL" + debugDisplay,
                liteLastName: "DUPOND",
                liteDateOfBirth: "27/04/1988",
              },
            ],
            static: {
              header: null,
              message: null,
              annexe: null,
              isBlacklisted: false,
              signature: null,
            },
            external: [],
          },
          errors: [],
        };
        return res.status(200).send(response);
      }
      if (debugDisplay === 2) {
        const response: ApiResponse = {
          resourceType: "DCC_VACCINATION",
          data: {
            dynamic: [
              {
                liteValidityStatus: "Non valide",
                liteFirstName: "MICHEL" + debugDisplay,
                liteLastName: "DUPOND",
                liteDateOfBirth: "27/04/1988",
              },
            ],
            static: {
              header: null,
              message: null,
              annexe: null,
              isBlacklisted: false,
              signature: null,
            },
            external: [],
          },
          errors: [],
        };
        return res.status(200).send(response);
      }
      if (debugDisplay === 3) {
        const response: ApiResponse = {
          resourceType: "DCC_VACCINATION",
          data: {
            dynamic: [
              {
                liteValidityStatus: "Non valide",
                liteFirstName: "MICHEL" + debugDisplay,
                liteLastName: "DUPOND",
                liteDateOfBirth: "27/04/1988",
              },
            ],
            static: {
              header: null,
              message: null,
              annexe: null,
              isBlacklisted: true,
              signature: null,
            },
            external: [],
          },
          errors: [],
        };
        return res.status(200).send(response);
      }

      return res.status(400).send({
        message: "You must provide the debug option in the body of the request",
        options: [
          "0 : normal behavior",
          "1 : valid certificate",
          "2 : invalid certificate",
          "3 : fraudulent certificate",
        ],
      });
    } catch (error) {
      return next(error);
    }
  };
}
